﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Lab2
{

    public partial class Form1 : Form
    {
        public string arrayIntring;
        int[] initialArray;
        string fileName;
        int sizeArray;

        public Form1()
        {
            InitializeComponent();
        }

        public int findMinOrMax(int[] array, string mode)
        {
            int value = array[0];
            int index = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (mode=="min") {
                    if (array[i] < value) {
                        value = array[i];
                        index = i;
                    }
                }
                else if (array[i] > value) {
                    value = array[i];
                    index = i;
                }
            }
            if (value < -1073741823 || value > 1073741823) throw new IndexOutOfRangeException();
            return index;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        

        bool CheckfileName(string name)
        {
            char[] nameFileCharArray = name.ToCharArray();
            Array.Reverse(nameFileCharArray);
            int i = 0;
            if (nameFileCharArray.Length >= 4 && nameFileCharArray[i] == 't' && nameFileCharArray[i + 1] == 'x' && nameFileCharArray[i + 2] == 't' && nameFileCharArray[i + 3] == '.')
            {
                return true;
            }
            else
                return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            arrayIntring = textBox1.Text.Replace(" ", "");
           
            try
            {
                if (arrayIntring[arrayIntring.Length - 1] == ',') arrayIntring = arrayIntring.Substring(0, arrayIntring.Length - 1);
                string[] numbers = arrayIntring.Split(',');
                initialArray = new int[numbers.Length];
                for (int i = 0; i < numbers.Length; i++)
                {
                    initialArray[i] = int.Parse(numbers[i]);
                }
                int indexMinItem = findMinOrMax(initialArray, "min");
                int indexMaxItem = findMinOrMax(initialArray, "max");

                int[] resultArray= new int[numbers.Length];;
                for (int i = 0; i < numbers.Length; i++)
                {
                    if (i >= indexMinItem && i <= indexMaxItem && indexMinItem<indexMaxItem)
                    {
                        for (int j = indexMaxItem; j >= indexMinItem; j--, i++)
                        {
                            resultArray[i] = initialArray[j];
                            if (indexMinItem == j) i--;
                        }
                    } else if (i >= indexMaxItem && i <= indexMinItem && indexMinItem > indexMaxItem)
                    {
                        for (int j = indexMinItem; j >= indexMaxItem; j--, i++)
                        {
                            resultArray[i] = initialArray[j];
                            if (indexMaxItem == j) i--;
                        }
                    }
                    else resultArray[i] = initialArray[i];
                }

                string result="";
                for (int i = 0; i < numbers.Length; i++)
                {
                    result += resultArray[i].ToString();
                    if (i != numbers.Length - 1) result += ", ";
                }
                if (radioButtonResInFile.Checked)
                textBox2.Text = result;
                if (radioButton2.Checked)
                {
                    WriteFile(result);
                    textBox2.Clear();
                }
            }
            catch (Exception ex)
            {
                if (textBox1.Text == "")
                {
                    var messageWrongFileFormat = MessageBox.Show("Вы не ввели исходный массив", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    var messageWrongFileFormat = MessageBox.Show("Ожидались целые числа в диапазоне от -1073741823 до 1073741823 включительно!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                initialArray = null;
                textBox1.Clear();
                textBox2.Clear();
            }
        }

        private void load2(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void load_Click(object sender, EventArgs e)
        {
            fileName = null;
            textBox1.Clear();
            textBox2.Clear();
            initialArray = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = " |*.txt";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileName = openFileDialog.FileName;

                if (CheckfileName(fileName))
                {
                    StreamReader streamReader = new StreamReader(openFileDialog.OpenFile());
                    string strArray = streamReader.ReadLine();
                    if (strArray != null)
                    {
                        if (strArray.Length <= 1000)
                        {
                            try
                            {
                                arrayIntring = strArray.Replace(" ", "");
                                if (arrayIntring[arrayIntring.Length - 1] == ',') arrayIntring = arrayIntring.Substring(0, arrayIntring.Length - 1);
                                string[] numbers = arrayIntring.Split(',');

                                initialArray = new int[numbers.Length];
                                for (int i = 0; i < numbers.Length; i++)
                                {
                                    initialArray[i] = int.Parse(numbers[i]);
                                }
                                int indexMinItem = findMinOrMax(initialArray, "min");
                                int indexMaxItem = findMinOrMax(initialArray, "max");

                                int[] resultArray = new int[numbers.Length]; ;
                                for (int i = 0; i < numbers.Length; i++)
                                {
                                    if (i >= indexMinItem && i <= indexMaxItem && indexMinItem < indexMaxItem)
                                    {
                                        for (int j = indexMaxItem; j >= indexMinItem; j--, i++)
                                        {
                                            resultArray[i] = initialArray[j];
                                            if (indexMinItem == j) i--;
                                        }
                                    }
                                    else if (i >= indexMaxItem && i <= indexMinItem && indexMinItem > indexMaxItem)
                                    {
                                        for (int j = indexMinItem; j >= indexMaxItem; j--, i++)
                                        {
                                            resultArray[i] = initialArray[j];
                                            if (indexMaxItem == j) i--;
                                        }
                                    }
                                    else resultArray[i] = initialArray[i];
                                }

                                string result = "";
                                for (int i = 0; i < numbers.Length; i++)
                                {
                                    result += resultArray[i].ToString();
                                    if (i != numbers.Length - 1) result += ", ";
                                }
                                textBox1.Text = arrayIntring;
                                textBox2.Text = result;
                            }
                            catch (Exception ex)
                            {
                                if (textBox1.Text == "")
                                {
                                    var messageWrongFileFormat = MessageBox.Show("Вы не ввели исходный массив", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                else
                                {
                                    var messageWrongFileFormat = MessageBox.Show("Ожидались целые числа в диапазоне от -1073741823 до 1073741823 включительно!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                initialArray = null;
                                textBox1.Clear();
                                textBox2.Clear();
                            }
                        }
                        else
                        {
                            string message = "Файл очень большой. Количество символов в файле не должно превышать 1000";
                            string caption = "Ошибка!";
                            var messageWrongFileFormat = MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            load2(sender, e);
                            textBox1.Clear();
                            textBox2.Clear();
                        }
                    }
                    else
                    {
                        var messageWrongFileFormat = MessageBox.Show("Файл пуст!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        load2(sender, e);
                        textBox1.Clear();
                        textBox2.Clear();
                    }
                    streamReader.Dispose();
                    streamReader.Close();
                }
                else
                {
                    var messageWrongFileFormat = MessageBox.Show("Расширение файла должно быть txt", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    load2(sender, e);
                    textBox1.Clear();
                    textBox2.Clear();
                }
            }
            else
            {
                var messageWrongFileFormat = MessageBox.Show("Неверные данные в массиве", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                initialArray = null;
                textBox2.Clear();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            Random random = new Random();
            int sizeArray = 10;
            initialArray = new int[sizeArray];
            for (int i = 0; i < sizeArray; i++)
            {
                initialArray[i] = random.Next(-100, 100);
                textBox1.Text += initialArray[i].ToString() + ", ";
            }
        }

        private void WriteFile(string result)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Сохранить массив";
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.Filter = " |*.txt";
            saveFileDialog.ShowHelp = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string nameSaveFile = saveFileDialog.FileName;

                if (CheckfileName(nameSaveFile))
                {
                    if (fileName == null || fileName != nameSaveFile) // сравниваем не является ли сохраненный файл, файлом из которого мы загрузили массив
                    {
                        StreamWriter writer = new StreamWriter(saveFileDialog.OpenFile());
                        writer.Write(result);
                        writer.Dispose();
                        writer.Close();
                        var messageWrongFileFormat = MessageBox.Show("Файл " + nameSaveFile + " сохранен!", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        var messageWrongFileFormat = MessageBox.Show("Файл не сохранен! Нельзя сохранить массив в файле, из которого он был считан! ", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        WriteFile(result);
                    }

                }
                else
                {
                    var messageWrongFileFormat = MessageBox.Show("Вы указали неверный формат файла. Необходимо указать файл с расширением txt", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    WriteFile(result);
                }
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButtonResInFile_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
